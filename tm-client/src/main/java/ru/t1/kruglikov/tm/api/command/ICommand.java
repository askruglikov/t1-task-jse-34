package ru.t1.kruglikov.tm.api.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ICommand {

    void execute();

    @Nullable
    String getArgument();

    @NotNull
    String getName();

    @NotNull
    String getDescription();

}
