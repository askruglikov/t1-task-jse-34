package ru.t1.kruglikov.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.model.Project;

public final class ProjectCompleteByIdResponse extends AbstractProjectResponse {

    public ProjectCompleteByIdResponse(@Nullable Project project) {
        super(project);
    }

}
