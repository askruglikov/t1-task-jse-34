package ru.t1.kruglikov.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.model.Project;

public final class ProjectShowByIdResponse extends AbstractProjectResponse {

    public ProjectShowByIdResponse(@Nullable Project project) {
        super(project);
    }

}
