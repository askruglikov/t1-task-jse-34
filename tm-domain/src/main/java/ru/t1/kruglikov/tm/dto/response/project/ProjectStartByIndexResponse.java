package ru.t1.kruglikov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.model.Project;

@NoArgsConstructor
public final class ProjectStartByIndexResponse extends AbstractProjectResponse {

    public ProjectStartByIndexResponse(@Nullable Project project) {
        super(project);
    }

}
