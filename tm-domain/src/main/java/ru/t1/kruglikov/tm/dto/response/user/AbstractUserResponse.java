package ru.t1.kruglikov.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.response.AbstractResponse;
import ru.t1.kruglikov.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private User user;

    public AbstractUserResponse(@Nullable User user) {
        this.user = user;
    }

}
