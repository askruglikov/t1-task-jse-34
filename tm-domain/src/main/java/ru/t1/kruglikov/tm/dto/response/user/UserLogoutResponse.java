package ru.t1.kruglikov.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.model.User;

@NoArgsConstructor
public final class UserLogoutResponse extends AbstractUserResponse {

    public UserLogoutResponse(@Nullable User user) {
        super(user);
    }

}
