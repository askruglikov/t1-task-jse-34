package ru.t1.kruglikov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.api.model.IWBS;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.exception.entity.UserNotFoundException;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements IWBS {

    @NotNull private String name = "";
    @NotNull private String description = "";
    @NotNull private Status status = Status.NOT_STARTED;
    @Nullable private String projectId;
    @NotNull private Date created = new Date();

    public Task(@Nullable final User user,
                @NotNull final String name,
                @NotNull final String description,
                @NotNull final Status status
    ) {
        if (user == null) throw new UserNotFoundException();
        this.setUserId(user.getId());
        this.name = name;
        this.description = description;
        this.status = status;
    }

}
