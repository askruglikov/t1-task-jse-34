package ru.t1.kruglikov.tm.api.repository;

import ru.t1.kruglikov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}