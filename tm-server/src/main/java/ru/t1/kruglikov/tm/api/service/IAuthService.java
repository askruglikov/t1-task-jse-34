package ru.t1.kruglikov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.model.Session;
import ru.t1.kruglikov.tm.model.User;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    Session validateToken(@Nullable String token);

    void logout(@Nullable final String token);

}

