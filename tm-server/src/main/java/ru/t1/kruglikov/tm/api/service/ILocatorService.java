package ru.t1.kruglikov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.kruglikov.tm.api.endpoint.IDomainEndpoint;

public interface ILocatorService {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    ISessionService getSessionService();

}
