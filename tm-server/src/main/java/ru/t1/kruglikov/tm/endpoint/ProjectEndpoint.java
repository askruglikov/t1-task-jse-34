package ru.t1.kruglikov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.kruglikov.tm.api.service.ILocatorService;
import ru.t1.kruglikov.tm.api.service.IProjectService;
import ru.t1.kruglikov.tm.api.service.IProjectTaskService;
import ru.t1.kruglikov.tm.dto.request.project.*;
import ru.t1.kruglikov.tm.dto.response.project.*;
import ru.t1.kruglikov.tm.enumerated.ProjectSort;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.model.Project;
import ru.t1.kruglikov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService(endpointInterface = "ru.t1.kruglikov.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final ILocatorService locatorService) {
        super(locatorService);
    }

    @NotNull @Override @WebMethod
    public ProjectChangeStatusByIdResponse changeStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final Status status = request.getStatus();

        @Nullable final Project project = getLocatorService().getProjectService().changeProjectStatusById(userId, projectId, status);

        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull @Override @WebMethod
    public ProjectChangeStatusByIndexResponse changeStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();

        @Nullable final Project project = getLocatorService().getProjectService().changeProjectStatusByIndex(userId, index, status);

        return new ProjectChangeStatusByIndexResponse(project);
    }

    @NotNull @Override @WebMethod
    public ProjectClearResponse clear(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();

        getLocatorService().getProjectService().removeAll(userId);

        return new ProjectClearResponse();
    }

    @NotNull @Override @WebMethod
    public ProjectCompleteByIdResponse completeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();

        @Nullable final Project project = getLocatorService().getProjectService().changeProjectStatusById(userId, projectId, Status.COMPLETED);

        return new ProjectCompleteByIdResponse(project);
    }

    @NotNull @Override @WebMethod
    public ProjectCompleteByIndexResponse completeByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();

        @Nullable final Project project = getLocatorService().getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);

        return new ProjectCompleteByIndexResponse(project);
    }

    @NotNull @Override @WebMethod
    public ProjectCreateResponse create(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();

        @Nullable final Project project = getLocatorService().getProjectService().create(userId, name, description);

        return new ProjectCreateResponse(project);
    }

    @NotNull @Override @WebMethod
    public ProjectListResponse list(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectListRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final ProjectSort sort = request.getSort();

        @Nullable final Comparator<Project> comparator = sort != null ? sort.getComparator() : null;
        @Nullable final List<Project> projects = getLocatorService().getProjectService().findAll(userId, comparator);

        @NotNull final ProjectListResponse response = new ProjectListResponse();
        response.setProjects(projects);
        return response;
    }

    @NotNull @Override @WebMethod
    public ProjectRemoveByIdResponse removeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();

        getLocatorService().getProjectTaskService().removeProjectById(userId, projectId);

        return new ProjectRemoveByIdResponse();
    }

    @NotNull @Override @WebMethod
    public ProjectRemoveByIndexResponse removeByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();

        @Nullable final Project project = getLocatorService().getProjectService().findOneByIndex(userId, index);
        getLocatorService().getProjectTaskService().removeProjectById(userId, project.getId());

        return new ProjectRemoveByIndexResponse();
    }

    @NotNull @Override @WebMethod
    public ProjectShowByIdResponse showById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();

        @Nullable final Project project = getLocatorService().getProjectService().findOneById(userId, projectId);

        return new ProjectShowByIdResponse(project);
    }

    @NotNull @Override @WebMethod
    public ProjectShowByIndexResponse showByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();

        @Nullable final Project project = getLocatorService().getProjectService().findOneByIndex(userId, index);

        return new ProjectShowByIndexResponse(project);
    }

    @NotNull @Override @WebMethod
    public ProjectStartByIdResponse startById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();

        @Nullable final Project project = getLocatorService().getProjectService().changeProjectStatusById(userId, projectId, Status.IN_PROGRESS);

        return new ProjectStartByIdResponse(project);
    }

    @NotNull @Override @WebMethod
    public ProjectStartByIndexResponse startByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();

        @Nullable final Project project = getLocatorService().getProjectService().changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);

        return new ProjectStartByIndexResponse(project);
    }

    @NotNull @Override @WebMethod
    public ProjectUpdateByIdResponse updateById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();

        @Nullable final Project project = getLocatorService().getProjectService().updateById(userId, projectId, name, description);

        return new ProjectUpdateByIdResponse(project);
    }

    @NotNull @Override @WebMethod
    public ProjectUpdateByIndexResponse updateByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();

        @Nullable final Project project = getLocatorService().getProjectService().updateByIndex(userId, index, name, description);

        return new ProjectUpdateByIndexResponse(project);
    }

}
