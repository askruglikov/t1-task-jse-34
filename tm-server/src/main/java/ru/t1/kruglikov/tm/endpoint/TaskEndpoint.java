package ru.t1.kruglikov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.kruglikov.tm.api.service.ILocatorService;
import ru.t1.kruglikov.tm.api.service.IProjectTaskService;
import ru.t1.kruglikov.tm.api.service.ITaskService;
import ru.t1.kruglikov.tm.dto.request.task.*;
import ru.t1.kruglikov.tm.dto.response.task.*;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.enumerated.TaskSort;
import ru.t1.kruglikov.tm.model.Session;
import ru.t1.kruglikov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService(endpointInterface = "ru.t1.kruglikov.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final ILocatorService locatorService) {
        super(locatorService);
    }

    @NotNull @Override @WebMethod
    public TaskBindToProjectResponse bindToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();

        getLocatorService().getProjectTaskService().bindTaskToProject(userId, projectId, taskId);

        return new TaskBindToProjectResponse();
    }

    @NotNull @Override @WebMethod
    public TaskChangeStatusByIdResponse changeStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Status status = request.getStatus();

        @Nullable final Task task = getLocatorService().getTaskService().changeTaskStatusById(userId, taskId, status);

        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull @Override @WebMethod
    public TaskChangeStatusByIndexResponse changeStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();

        @Nullable final Task task = getLocatorService().getTaskService().changeTaskStatusByIndex(userId, index, status);

        return new TaskChangeStatusByIndexResponse(task);
    }

    @NotNull @Override @WebMethod
    public TaskClearResponse clear(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();

        getLocatorService().getTaskService().removeAll(userId);

        return new TaskClearResponse();
    }

    @NotNull @Override @WebMethod
    public TaskCompleteByIdResponse completeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();

        @Nullable final Task task = getLocatorService().getTaskService().changeTaskStatusById(userId, taskId, Status.COMPLETED);

        return new TaskCompleteByIdResponse(task);
    }

    @NotNull @Override @WebMethod
    public TaskCompleteByIndexResponse completeByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();

        @Nullable final Task task = getLocatorService().getTaskService().changeTaskStatusByIndex(userId, index, Status.COMPLETED);

        return new TaskCompleteByIndexResponse(task);
    }

    @NotNull @Override @WebMethod
    public TaskCreateResponse create(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();

        @Nullable final Task task = getLocatorService().getTaskService().create(userId, name, description);

        return new TaskCreateResponse(task);
    }

    @NotNull @Override @WebMethod
    public TaskListByProjectIdResponse listByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListByProjectIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();

        @NotNull final List<Task> tasks = getLocatorService().getTaskService().findAllByProjectId(userId, projectId);

        @NotNull final TaskListByProjectIdResponse response = new TaskListByProjectIdResponse();
        response.setTasks(tasks);
        return response;
    }

    @NotNull @Override @WebMethod
    public TaskListResponse list(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final TaskSort sort = request.getSort();

        @Nullable final Comparator<Task> comparator = sort != null ? sort.getComparator() : null;
        @NotNull final List<Task> tasks = getLocatorService().getTaskService().findAll(userId, comparator);

        @NotNull final TaskListResponse response = new TaskListResponse();
        response.setTasks(tasks);
        return response;
    }

    @NotNull @Override @WebMethod
    public TaskRemoveByIdResponse removeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();

        @Nullable final Task task = getLocatorService().getTaskService().removeOneById(userId, taskId);

        return new TaskRemoveByIdResponse(task);
    }

    @NotNull @Override @WebMethod
    public TaskRemoveByIndexResponse removeByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();

        @Nullable final Task task = getLocatorService().getTaskService().removeOneByIndex(userId, index);

        return new TaskRemoveByIndexResponse(task);
    }

    @NotNull @Override @WebMethod
    public TaskShowByIdResponse showById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();

        @Nullable final Task task = getLocatorService().getTaskService().findOneById(userId, taskId);

        return new TaskShowByIdResponse(task);
    }

    @NotNull @Override @WebMethod
    public TaskShowByIndexResponse showByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();

        @Nullable final Task task = getLocatorService().getTaskService().findOneByIndex(userId, index);

        return new TaskShowByIndexResponse(task);
    }

    @NotNull @Override @WebMethod
    public TaskStartByIdResponse startById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();

        @Nullable final Task task = getLocatorService().getTaskService().changeTaskStatusById(userId, taskId, Status.IN_PROGRESS);

        return new TaskStartByIdResponse(task);
    }

    @NotNull @Override @WebMethod
    public TaskStartByIndexResponse startByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();

        @Nullable final Task task = getLocatorService().getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);

        return new TaskStartByIndexResponse(task);
    }

    @NotNull @Override @WebMethod
    public TaskUnbindFromProjectResponse unbindFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();

        getLocatorService().getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);

        return new TaskUnbindFromProjectResponse();
    }

    @NotNull @Override @WebMethod
    public TaskUpdateByIdResponse updateById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();

        @Nullable final Task task = getLocatorService().getTaskService().updateById(userId, taskId, name, description);

        return new TaskUpdateByIdResponse(task);
    }

    @NotNull @Override @WebMethod
    public TaskUpdateByIndexResponse updateByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();

        @Nullable final Task task = getLocatorService().getTaskService().updateByIndex(userId, index, name, description);

        return new TaskUpdateByIndexResponse(task);
    }

}
