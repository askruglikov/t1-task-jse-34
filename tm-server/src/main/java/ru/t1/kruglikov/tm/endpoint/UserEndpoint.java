package ru.t1.kruglikov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.api.endpoint.IUserEndpoint;
import ru.t1.kruglikov.tm.api.service.ILocatorService;
import ru.t1.kruglikov.tm.api.service.IUserService;
import ru.t1.kruglikov.tm.dto.request.user.*;
import ru.t1.kruglikov.tm.dto.response.user.*;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.model.Session;
import ru.t1.kruglikov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.kruglikov.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final ILocatorService locatorService) {
        super(locatorService);
    }

    @NotNull @Override @WebMethod
    public UserChangePasswordResponse changePassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();

        @Nullable final User user = getLocatorService().getUserService().setPassword(userId, password);

        return new UserChangePasswordResponse(user);
    }

    @NotNull @Override @WebMethod
    public UserLockResponse lock(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();

        @Nullable final User user = getLocatorService().getUserService().lockOneByLogin(login);

        return new UserLockResponse(user);
    }

    @NotNull @Override @WebMethod
    public UserRegistryResponse registry(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRegistryRequest request
    ) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();

        @Nullable final User user = getLocatorService().getUserService().create(login, password, email);

        return new UserRegistryResponse(user);
    }

    @NotNull @Override @WebMethod
    public UserRemoveResponse remove(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();

        @Nullable final User user = getLocatorService().getUserService().removeOneByLogin(login);

        return new UserRemoveResponse(user);
    }

    @NotNull @Override @WebMethod
    public UserUnlockResponse unlock(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();

        @Nullable final User user = getLocatorService().getUserService().unlockOneByLogin(login);

        return new UserUnlockResponse(user);
    }

    @NotNull @Override @WebMethod
    public UserUpdateProfileResponse updateProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateProfileRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();

        @Nullable final User user = getLocatorService().getUserService().updateUser(userId, firstName, lastName, middleName);

        return new UserUpdateProfileResponse(user);
    }

    @NotNull @Override @WebMethod
    public UserViewProfileResponse viewProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserViewProfileRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();

        @Nullable final User user = getLocatorService().getUserService().findOneById(userId);

        return new UserViewProfileResponse(user);
    }

}
