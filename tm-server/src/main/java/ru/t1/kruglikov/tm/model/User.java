package ru.t1.kruglikov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.enumerated.Role;

@Getter
@Setter
public class User extends AbstractModel {

    @Nullable private String login;
    @Nullable private String passwordHash;
    @Nullable private String email;
    @Nullable private String firstName;
    @Nullable private String lastName;
    @Nullable private String middleName;
    @NotNull private Role role = Role.USUAL;
    @NotNull private Boolean locked = false;

}
