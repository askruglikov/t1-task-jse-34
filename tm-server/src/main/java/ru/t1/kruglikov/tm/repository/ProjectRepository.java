package ru.t1.kruglikov.tm.repository;

import ru.t1.kruglikov.tm.api.repository.IProjectRepository;
import ru.t1.kruglikov.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {
}
