package ru.t1.kruglikov.tm.repository;

import ru.t1.kruglikov.tm.api.repository.ISessionRepository;
import ru.t1.kruglikov.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}

