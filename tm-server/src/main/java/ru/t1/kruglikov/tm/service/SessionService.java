package ru.t1.kruglikov.tm.service;


import org.jetbrains.annotations.NotNull;
import ru.t1.kruglikov.tm.api.repository.ISessionRepository;
import ru.t1.kruglikov.tm.api.service.ISessionService;
import ru.t1.kruglikov.tm.model.Session;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull ISessionRepository repository) {
        super(repository);
    }

}
